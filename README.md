**Problem 1: Running Sum of 1d Array**

Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
Return the running sum of nums.

Level: easy

Running command example: python3 RunningSumof1dArray.py 3 1 2 10 1

Expected output: [3, 4, 6, 16, 17]
