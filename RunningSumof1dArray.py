import sys
from typing import List
class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        sumArr = []
        for i in range(len(nums)):
            if (i == 0):
                sumArr.append(nums[i])
            else:
                sumArr.append(nums[i]+sumArr[i-1])
        
        return sumArr

arg = sys.argv[1:]
arg_list = [int(i) for i in arg]
# print(arg_list)
sol = Solution()
print(sol.runningSum(arg_list))
            